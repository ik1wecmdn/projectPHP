<!DOCTYPE html>
<html>
<head>
	<title>Latihan 2  - Connect MySQL</title>
	<style>
		table{
			border:1px solid #2F2F2F;
			width: 100%;
			border-collapse: collapse;
		}
		th,td{
			border: 1px solid #2F2F2F;
			padding: 2px 5px;
		}
		thead{
			background-color: #421800;
			color: #E3E3E3;
		}
		tbody tr:nth-child(odd){
			background-color: #FFEBDA;
		}
		tbody tr:nth-child(even){
			background-color: #CDF7D1;
		}
		.foto{
			width: 100px;
		}
		.rata-kanan{
			text-align:right;
		}
	</style>
	<script src="js/jquery-3.3.1.min.js"></script>
</head>
<body>
	<h1>MySQL DATA</h1>
	<a href="latihan3.php">Add Data</a>
	<hr>
	<form>
		Search : 
		<input type="text" name="search" value="<?php if(isset($_GET["search"])) echo $_GET["search"] ?>">
	</form>
	<br>
	<table>
		<thead>
			<tr>
				<th>ID MENU</th>
				<th>NAMA MENU</th>
				<th>KATEGORI</th>
				<th>HARGA</th>
				<th>STOK</th>
				<th>FOTO</th>
				<th>OPERATION</th>
			</tr>
		</thead>
		<tbody>
			<?php
			//kriteria untuk parameter
			$kriteria = "";
			if(isset($_GET["search"])){
				$kriteria = $_GET["search"];
			}
			
			require("koneksi.php");
			$db = new MyDatabase();
			$dtMenu = $db->GetData("SELECT id_menu,nama_menu,kategori,harga,stok FROM tb_menu WHERE nama_menu LIKE :kriteria",
			array(":kriteria"=> "%$kriteria%"));
			
			//perulangan perbaris membuat table row (tr)
			foreach($dtMenu as $row){
			?>
			<tr>
				<td><?php echo $row['id_menu'] ?></td>
				<td><?php echo $row['nama_menu'] ?></td>
				<td><?php echo $row['kategori'] ?></td>
				<td class="rata-kanan"><?php echo number_format($row['harga']) ?></td>
				<td class="rata-kanan"><?php echo $row['stok'] ?></td>
				<td><img class="foto" src="latihan4getimagemenu.php?id=<?php echo $row['id_menu']?>"></td>
				<td>
					<a href="latihan3.php?edit=<?php echo $row['id_menu'] ?>">Edit</a>  |  
					<a class="btnHapus" href="latihan4hapus.php?hapus=<?php echo $row['id_menu'] ?>">Hapus</a>
				</td>
			</tr>
			<?php	
			} //ttup foreach row
			
			
			//tutup mysql
			$koneksi = null;
			?>
			
			
		</tbody>
	</table>
	
	<script>
		$(".btnHapus").click(function(e){
						
			var jawab = confirm("Yakin mau dihapus??");
			if(jawab == true){
				//proses hapus dilanjutkan
			}else{
				//batalkan proses click
				e.preventDefault();
			}
		});
	</script>
	
</body>
</html>
