<!DOCTYPE html>
<html>
	<head>
		<title>Latihan 1</title>
		<style>
			label{
				width: 200px;
				display:block;
				float:left;
			}
			.group{
				margin-bottom:5px;
			}
			.btnSubmit{
				margin-left:200px;
				margin-top:15px;
				line-height:26px;
			}
		</style>
	</head>
	<body>
		<form method="post">
			<h1>Form Pendaftaran</h1>
			<div class="group">
				<label>NAMA LENGKAP</label>
				<input type="text" name="txtNama">
			</div>
			<div class="group">
				<label>ALAMAT</label>
				<textarea name="txtAlamat"></textarea>
			</div>
			<div class="group">
				<label>JENIS KELAMIN</label>
				<input type="radio" value="L" name="rdoJK">LAKI LAKI
				<input type="radio" value="P" name="rdoJK">PEREMPUAN
			</div>
			<div class="group">
				<label>TANGGAL LAHIR</label>
				<select name="cmbTgl">
					<?php
					//perulangan tanggal 1 sd 31
					for($i=1; $i<=31; $i++){
						echo '<option>' . $i . '</option>';
					}
					?>
				</select>
				<select name="cmbBulan">
					<?php
					//buat array bulan
					$bulan = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
					for($i=1; $i<=12; $i++){
						echo '<option value="' . $i . '">' . $bulan[$i-1] . '</option>';
					}
					?>
				</select>
				<select name="cmbTahun">
					<?php 
					//ambil tahun menggunakan fungsi date(format)
					$tahun = date('Y');
					for($i=$tahun; $i>=$tahun-100; $i--){
						echo "<option>$i</option>";
					}
					?>
				</select>
			</div>
			<input class="btnSubmit" name="submit" type="submit" value="SUBMIT">
		</form>
		
		<?php
		//jika sudah di post variabel submit (sudah ditekan submit)
		if(isset($_POST['submit'])){
			//cek jenis kelamin
			if($_POST['rdoJK'] == 'L'){
				$sal = "Mas";
			}else{
				$sal = "Mbak";
			}
			//gabung dan tampilkan
			echo "<h1>Selamat data dengan nama $sal {$_POST['txtNama']} telah disimpan</h1>";
		}
		?>
		
	</body>
</html>