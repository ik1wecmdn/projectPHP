<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wellcome to Informatika 2017</title>
    <!-- css bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- additional css lib -->
    <!--<link rel="stylesheet" href="css/slimmenu.min.css">-->
    <link rel="stylesheet" href="css/slicknav.min.css">
    
    <!-- tambah custom css di sini-->
    <link rel="stylesheet" href="css/my-style.css">
    
    <!-- tambahkan jquery -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <!-- tambahkan popper untuk dropdown -->
	<script src="js/popper.min.js"></script>
    <!-- tambahkan js bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- additional lib -->
    <!--<script src="js/jquery.slimmenu.min.js"></script>-->
    <script src="js/jquery.slicknav.min.js"></script>
    
  </head>
  <body>
  	
  	
  	<?php include("header.php"); ?>
  	
  	
  	<div class="my-slider">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-12">
  					<!--slider here-->
  					
  					<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
					  <ol class="carousel-indicators">
						<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					  </ol>
					  <div class="carousel-inner">
						<div class="carousel-item active">
						  <img class="d-block w-100" src="images/slide1.jpg" alt="First slide">
						</div>
						<div class="carousel-item">
						  <img class="d-block w-100" src="images/slide2.jpg" alt="Second slide">
						</div>
					  </div>
					  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					  </a>
					  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					  </a>
					</div>
  					<!--end slider -->
  				</div>
  			</div>
  		</div>
  	</div>
  	
  	<div class="my-product">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-4">
  					<!--product col 1-->
  					<div class="my-kotak">
  						<img src="images/logoik.jpg">
  						<h4>INFORMATIKA</h4>
  						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
  						<a class="btn btn-primary" href="#">Read More</a>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<!--product col 2-->
  					<div class="my-kotak">
  						<img src="images/logoik.jpg">
  						<h4>EXPORT IMPORT</h4>
  						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
  						<a class="btn btn-primary" href="#">Read More</a>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<!--product col 3-->
  					<div class="my-kotak">
  						<img src="images/logoik.jpg">
  						<h4>DESAIN</h4>
  						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
  						<a class="btn btn-primary" href="#">Read More</a>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
  	
  	<div class="my-content">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-4">
  					<!-- side menu -->
  					
  					<div class="my-side-menu">
  						<h2>LATEST NEWS</h2>
  						<a href="#">We will make best software</a>
  						<p>21 Maret 2005</p>
  						<img src="images/software.jpg">
  						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
  					</div>
  					
  					
  				</div>
  				<div class="col-md-8">
  					<!-- content utama -->
  					
  					<div class="my-main-content">
  						<h1>Wellcome to My Company</h1>
  						<img src="images/logo-wearnes.jpg">
  						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
  					</div>
  					
  				</div>
  			</div>
  		</div>
  	</div>
  	
  	<div class="my-footer">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-12">
  					<!--footer-->
  					&copy; 2017 by informatika 1 wearnes education center madiun
  				</div>
  			</div>
  		</div>
  	</div>
  	
  	
  	
  </body>
</html>
