<?php
if(isset($_GET["id"])){
	$edit = $_GET["id"];
}else{
	$edit = "";
}

if($edit == ""){
	//tambah baru
	$menu = array(
		"nama_menu" => "",
		"kategori"  => "",
		"harga"     => 0,
		"stok"      => 0,
		"satuan"    => ""
	);
}else{
	require("../koneksi.php");
	$db = new MyDatabase();
	$tbMenu = $db->GetData("SELECT * FROM tb_menu WHERE id_menu=:edit",array(":edit"=>$edit));
	$menu = $tbMenu[0];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Form Input Data Menu</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  
  <!-- summernote style -->
  <link href="vendor/summernote/summernote-bs4.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Input Data Menu</div>
      <div class="card-body">
        <form action="menu-save.php" method="post" enctype="multipart/form-data">
          <input type="hidden" name="edit" value="<?php echo $edit?>">
          <div class="form-group">
          	<label>Nama Menu :</label>
          	<input type="text" class="form-control" name="namaMenu" value="<?php echo $menu["nama_menu"] ?>">
          </div>
          <div class="form-group">
          	<label>Kategori : </label>
          	<input type="text" class="form-control" name="Kategori" value="<?php echo $menu["kategori"] ?>">
          </div>
          <div class="form-group">
          	<label>Harga : </label>
          	<input type="text" id="Harga" class="form-control" name="Harga" value="<?php echo $menu["harga"] ?>">
          </div>
          <div class="form-group">
          	<label>Stok : </label>
          	<input type="text" class="form-control" name="Stok" value="<?php echo $menu["stok"] ?>">
          </div>
          <div class="form-group">
          	<label>Satuan : </label>
          	<input type="text" class="form-control" name="Satuan" value="<?php echo $menu["satuan"] ?>">
          </div>
          <div class="form-group">
          	<label>Foto :</label>
          	<input type="file" name="Foto" >
          </div>
          <div class="form-group">
          	<label>Keterangan :</label>
          	<textarea id="Keterangan" class="form-control" name="Keterangan"></textarea>
          </div>
          <button class="btn btn-primary btn-block" type="submit">SIMPAN</button>
        </form>
        
        <div class="text-center">
          <a class="d-block small mt-3" href="menu.php">Back</a>
          
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  
  <!-- script summernote -->
  <script src="vendor/summernote/summernote-bs4.js"></script>
  <script>
	  $(document).ready(function(){
		  $('#Harga').on('keydown', function(e) {
	
  if (this.selectionStart || this.selectionStart == 0) {
	// selectionStart won't work in IE < 9
	
	var key = e.which;
	var prevDefault = true;
	
	var thouSep = ",";  // your seperator for thousands
	var deciSep = ".";  // your seperator for decimals
	var deciNumber = 2; // how many numbers after the comma
	
	var thouReg = new RegExp(thouSep,"g");
	var deciReg = new RegExp(deciSep,"g");
	
	function spaceCaretPos(val, cPos) {
		/// get the right caret position without the spaces
		
		if (cPos > 0 && val.substring((cPos-1),cPos) == thouSep)
		  cPos = cPos-1;
		
		if (val.substring(0,cPos).indexOf(thouSep) >= 0) {
		  cPos = cPos - val.substring(0,cPos).match(thouReg).length;
		}
		
		return cPos;
	}
	
	function spaceFormat(val, pos) {
		/// add spaces for thousands
		
		if (val.indexOf(deciSep) >= 0) {
			var comPos = val.indexOf(deciSep);
			var int = val.substring(0,comPos);
			var dec = val.substring(comPos);
		} else{
			var int = val;
			var dec = "";
		}
		var ret = [val, pos];
		
		if (int.length > 3) {
			
			var newInt = "";
			var spaceIndex = int.length;
			
			while (spaceIndex > 3) {
				spaceIndex = spaceIndex - 3;
				newInt = thouSep+int.substring(spaceIndex,spaceIndex+3)+newInt;
				if (pos > spaceIndex) pos++;
			}
			ret = [int.substring(0,spaceIndex) + newInt + dec, pos];
		}
		return ret;
	}
	
	$(this).on('keyup', function(ev) {
		
		if (ev.which == 8) {
			// reformat the thousands after backspace keyup
			
			var value = this.value;
			var caretPos = this.selectionStart;
			
			caretPos = spaceCaretPos(value, caretPos);
			value = value.replace(thouReg, '');
			
			var newValues = spaceFormat(value, caretPos);
			this.value = newValues[0];
			this.selectionStart = newValues[1];
			this.selectionEnd   = newValues[1];
		}
	});
	
	if ((e.ctrlKey && (key == 65 || key == 67 || key == 86 || key == 88 || key == 89 || key == 90)) ||
	   (e.shiftKey && key == 9)) // You don't want to disable your shortcuts!
		prevDefault = false;
	
	if ((key < 37 || key > 40) && key != 8 && key != 9 && prevDefault) {
		e.preventDefault();
		
		if (!e.altKey && !e.shiftKey && !e.ctrlKey) {
		
			var value = this.value;
			if ((key > 95 && key < 106)||(key > 47 && key < 58) ||
			  (deciNumber > 0 && (key == 110 || key == 188 || key == 190))) {
				
				var keys = { // reformat the keyCode
          48: 0, 49: 1, 50: 2, 51: 3,  52: 4,  53: 5,  54: 6,  55: 7,  56: 8,  57: 9,
          96: 0, 97: 1, 98: 2, 99: 3, 100: 4, 101: 5, 102: 6, 103: 7, 104: 8, 105: 9,
          110: deciSep, 188: deciSep, 190: deciSep
				};
				
				var caretPos = this.selectionStart;
				var caretEnd = this.selectionEnd;
				
				if (caretPos != caretEnd) // remove selected text
				value = value.substring(0,caretPos) + value.substring(caretEnd);
				
				caretPos = spaceCaretPos(value, caretPos);
				
				value = value.replace(thouReg, '');
				
				var before = value.substring(0,caretPos);
				var after = value.substring(caretPos);
				var newPos = caretPos+1;
				
				if (keys[key] == deciSep && value.indexOf(deciSep) >= 0) {
					if (before.indexOf(deciSep) >= 0) newPos--;
					before = before.replace(deciReg, '');
					after = after.replace(deciReg, '');
				}
				var newValue = before + keys[key] + after;
				
				if (newValue.substring(0,1) == deciSep) {
					newValue = "0"+newValue;
					newPos++;
				}
				
				while (newValue.length > 1 && newValue.substring(0,1) == "0" && newValue.substring(1,2) != deciSep) {
					newValue = newValue.substring(1);
					newPos--;
				}
				
				if (newValue.indexOf(deciSep) >= 0) {
					var newLength = newValue.indexOf(deciSep)+deciNumber+1;
					if (newValue.length > newLength) {
					  newValue = newValue.substring(0,newLength);
					}
				}
				
				newValues = spaceFormat(newValue, newPos);
				
				this.value = newValues[0];
				this.selectionStart = newValues[1];
				this.selectionEnd   = newValues[1];
			}
		}
	}
	
	$(this).on('blur', function(e) {
		
		if (deciNumber > 0) {
			var value = this.value;
			
			var noDec = "";
			for (var i = 0; i < deciNumber; i++) noDec += "0";
			
			if (value == "0" + deciSep + noDec) {
        this.value = ""; //<-- put your default value here
      } else if(value.length > 0) {
				if (value.indexOf(deciSep) >= 0) {
					var newLength = value.indexOf(deciSep) + deciNumber + 1;
					if (value.length < newLength) {
					  while (value.length < newLength) value = value + "0";
					  this.value = value.substring(0,newLength);
					}
				}
				else this.value = value + deciSep + noDec;
			}
		}
	});
  }
});
		  
		  $("#Keterangan").summernote({
			  height: 300
		  });
	  });
  </script>
  
  
</body>

</html>
