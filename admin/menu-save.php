<?php
require("../koneksi.php");
$db = new MyDatabase();

$foto = "";
if($_FILES["Foto"]["tmp_name"] != ""){
	//jika foto tidak kosong maka diambil dan convert ke base64text
	$foto = base64_encode(file_get_contents($_FILES["Foto"]["tmp_name"]));
}

//buat query
if($_POST["edit"] == ""){
	$sql = "INSERT INTO tb_menu (nama_menu,kategori,harga,stok,satuan,foto,keterangan) VALUES (:nama_menu,:kategori,:harga,:stok,:satuan,:foto,:keterangan)";
	$db->Execute($sql, array(
		":nama_menu" => $_POST["namaMenu"],
		":kategori"	 => $_POST["Kategori"],
		":harga"	 => $_POST["Harga"],
		":stok"		 => $_POST["Stok"],
		":satuan"	 => $_POST["Satuan"],
		":foto"      => $foto,
		":keterangan"=> $_POST['Keterangan']
	));
}else{
	$sql = "UPDATE tb_menu SET nama_menu=:nama_menu, kategori=:kategori, harga=:harga, stok=:stok, satuan=:satuan, keterangan=:keterangan WHERE id_menu=:edit";
	$db->Execute($sql,array(
		":nama_menu" => $_POST["namaMenu"],
		":kategori"	 => $_POST["Kategori"],
		":harga"	 => $_POST["Harga"],
		":stok"		 => $_POST["Stok"],
		":satuan"	 => $_POST["Satuan"],
		":keterangan"=> $_POST["Keterangan"],
		":edit"		 => $_POST["edit"]
	));
	//jika ada perubahan foto saja
	if($foto != ""){
		$db->Execute("UPDATE tb_menu SET foto=:foto WHERE id_menu=:edit", array(":foto"=>$foto,":edit"=>$_POST["edit"]));
	}
}

header("location:menu.php");

