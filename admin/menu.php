<?php
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login']!='sudah'){
	header('location:login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Daftar Menu</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  
  <link href="css/my-style.css" rel="stylesheet">
  
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  
  <?php include("navigation.php") ?>
  
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Menu</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <div class="header-width-button"><i class="fa fa-table"></i> Daftar Menu</div>
          <a href="form-menu.php" class="btn btn-primary float-right"><i class="fa fa-plus"></i> Add Menu Item</a>
        </div>
          
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nama</th>
                  <th>Kategori</th>
                  <th>Harga</th>
                  <th>Stok</th>
                  <th>Satuan</th>
                  <th>Operation</th>
                </tr>
              </thead>
              <tbody>
                <?php
				require("../koneksi.php");
				$db = new MyDatabase();
				$data = $db->GetData("SELECT id_menu,nama_menu,kategori,harga,stok,satuan FROM tb_menu");
				foreach($data as $row){
				?>
                <tr>
                  <td><?php echo $row["nama_menu"] ?></td>
                  <td><?php echo $row["kategori"] ?></td>
                  <td class="text-right"><?php echo number_format($row["harga"]) ?></td>
                  <td class="text-right"><?php echo $row["stok"] ?></td>
                  <td><?php echo $row["satuan"] ?></td>
                  <td>
					  <div class="btn-group">
						<a href="form-menu.php?id=<?php echo $row['id_menu'] ?>" class="btn btn-info"><i class="fa fa-edit"></i></a> 
						<a href="menu-hapus.php?id=<?php echo $row['id_menu'] ?>" class="btn btn-danger btn-hapus"><i class="fa fa-trash"></i></a>
					  </div>
                  </td>
                </tr>
                <?php
				} //tutupe foreach
				?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    
    <?php include("footer.php") ?>
    
    
    <script>
	$(document).ready(function() {
		
		$(".btn-hapus").click(function(e){
			var jawab = confirm("Yakin mau dihapus??");
			if(jawab == false){
				e.preventDefault();
			}
		});
		
  		$('#dataTable').DataTable();
	});
	</script>
    
  </div>
</body>

</html>
