<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wellcome to Informatika 2017</title>
    <!-- css bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- additional css lib -->
    <!--<link rel="stylesheet" href="css/slimmenu.min.css">-->
    <link rel="stylesheet" href="css/slicknav.min.css">
    <link rel="stylesheet" href="admin/vendor/summernote/summernote-bs4.css">
    
    
    <!-- tambah custom css di sini-->
    <link rel="stylesheet" href="css/my-style.css">
    
    
    <style>
		.my-content{
			margin-top:40px;
		}
	</style>
    
    
    <!-- tambahkan jquery -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <!-- tambahkan popper untuk dropdown -->
	<script src="js/popper.min.js"></script>
    <!-- tambahkan js bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- additional lib -->
    <!--<script src="js/jquery.slimmenu.min.js"></script>-->
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="admin/vendor/summernote/summernote-bs4.js"></script>
    
  </head>
  <body>
  	
  	<?php include("header.php") ?>
  	
  	<div class="my-content">
  		<div class="container">
  			<div class="row">
  				
  				<div class="col-md-8">
  					<!-- content utama -->
  					
  					<div class="my-main-content">
  						<h1 class="judul-content">Contact Us</h1>
  						<form>
						  <div class="form-group">
							<label for="inputEmail">Email address</label>
							<input type="email" class="form-control" id="inputEmail" aria-describedby="emailHelp">
							<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
						  </div>
						  <div class="form-group">
							<label for="inputNama">Nama Lengkap</label>
							<input type="text" class="form-control" id="inputNama">
						  </div>
						  <div class="form-group">
						  	<label>Pesan</label>
						  	<textarea class="form-control" id="inputPesan"></textarea>
						  </div>
						  
						  
						  <button type="submit" class="btn btn-primary">Submit</button>
						</form>
  						
					</div>
  				</div>
  				<div class="col-md-4">
  					<!-- side menu -->
  					
  					<div class="my-side-menu">
  						<h2>LATEST NEWS</h2>
  						<a href="#">We will make best software</a>
  						<p>21 Maret 2005</p>
  						<img src="images/software.jpg">
  						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
  					</div>
  					
  					
  				</div>
  			</div>
  		</div>
  	</div>
  	
  	<div class="my-footer">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-12">
  					<!--footer-->
  					&copy; 2017 by informatika 1 wearnes education center madiun
  				</div>
  			</div>
  		</div>
  	</div>
  	
  	<script>
		$("textarea").summernote({height:300});
	</script>
  	
  </body>
</html>
