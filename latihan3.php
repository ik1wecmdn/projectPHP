<?php
if(isset($_GET["edit"])){
	$edit = $_GET["edit"];
}else{
	$edit = "";
}

if($edit == ""){
	//tambah baru
	$menu = array(
		"nama_menu" => "",
		"kategori"  => "",
		"harga"     => 0,
		"stok"      => 0,
		"satuan"    => ""
	);
}else{
	require("koneksi.php");
	$db = new MyDatabase();
	$tbMenu = $db->GetData("SELECT * FROM tb_menu WHERE id_menu=:edit",array(":edit"=>$edit));
	$menu = $tbMenu[0];
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Latihan 3 - Add Data</title>
		<style>
			.group{
				margin-bottom: 5px;
			}
			label{
				display: block;
				width: 100px;
				float: left;
			}
			.btnSubmit{
				margin-left:107px;
			}
			.pesan{
				background-color: #FF4C4F;
				font-size: 20px;
				font-weight: bold;
				color:wheat;
				padding:5px;
				margin-bottom: 20px;
			}
		</style>
		<script src="js/jquery-3.3.1.min.js"></script>
	</head>
	<body>
		<h1>Form Tambah Data</h1>
		<hr>
		<div id="tempatPesan">
			<?php
			//tempat pesan yang di kirim dari php save ketika ada yang kosong
			if(isset($_GET["pesan"])){
				$pesan = $_GET["pesan"];
				echo '<p class="pesan">'.$pesan.'<p>';
			}
			?>
		</div>
		<!-- 
	     form id digunakan javascript untuk validasi
		 form method gunakan POST/GET (post recomended)
		 form action adalah file php yang digunakan untuk menyimpan ke mysql
		-->
		<form id="formInputMenu" method="post" action="latihan3save.php" enctype="multipart/form-data">
			<input type="hidden" name="edit" value="<?php echo $edit?>">
			<div class="group">
				<label>Nama Menu</label>:
				<!-- 
				 input id untuk javascript bisa didapat dengan $("#id")
				 input name untuk php bisa didapat dengan $_POST["name"]
				-->
				<input type="text" id="namaMenu" name="namaMenu" value="<?php echo $menu["nama_menu"] ?>">
			</div>
			<div class="group">
				<label>Kategori</label>:
				<input type="text" id="Kategori" name="Kategori" value="<?php echo $menu["kategori"] ?>">
			</div>
			<div class="group">
				<label>Harga</label>:
				<input type="number" id="Harga" name="Harga" value="<?php echo $menu["harga"] ?>">
			</div>
			<div class="group">
				<label>Stok</label>:
				<input type="number" id="Stok" name="Stok" value="<?php echo $menu["stok"] ?>">
			</div>
			<div class="group">
				<label>Satuan</label>:
				<input type="text" id="Satuan" name="Satuan" value="<?php echo $menu["satuan"] ?>">
			</div>
			<div class="group">
				<label>Foto</label>
				<input type="file" id="Foto" name="Foto">
			</div>
			<br>
			<!-- submit untuk simpan, reset untuk membatalkan inputan -->
			<input class="btnSubmit" type="submit" value="SIMPAN">
			<input type="reset" value="BATAL">
		</form>
		
		<script>
			//validasi dari sisi javascript
			//event ketika form di submit
			$("#formInputMenu").submit(function(e){
				//cek value dari namaMenu jika masih kosong
				if($("#namaMenu").val() == ""){
					$("#tempatPesan").html('<p class="pesan">Nama Belum di isi</p>');
					//batalkan proses submit
					e.preventDefault();
					return;
				}
				//cek value dari kategori
				if($("#Kategori").val() == ""){
					$("#tempatPesan").html('<p class="pesan">Kategori Belum di isi</p>');
					//batalkan proses submit
					e.preventDefault();
					return;
				}

				//jika sudah terisi semua maka proses submit akan diteruskan ke php save
			});
		</script>
	</body>
</html>