<?php
//validasi input dari sisi php
if($_POST["namaMenu"]==""){
	header("location:latihan3.php?pesan=Nama Belum di isi");
	return;
}
if($_POST["Kategori"]==""){
	header("location:latihan3.php?pesan=Kategori belum di isi");
	return;
}

require("koneksi.php");
$db = new MyDatabase();

$foto = "";
if($_FILES["Foto"]["tmp_name"] != ""){
	//jika foto tidak kosong maka diambil dan convert ke base64text
	$foto = base64_encode(file_get_contents($_FILES["Foto"]["tmp_name"]));
}

//buat query
if($_POST["edit"] == ""){
	$sql = "INSERT INTO tb_menu (nama_menu,kategori,harga,stok,satuan,foto) VALUES (:nama_menu,:kategori,:harga,:stok,:satuan,:foto)";
	$db->Execute($sql, array(
		":nama_menu" => $_POST["namaMenu"],
		":kategori"	 => $_POST["Kategori"],
		":harga"	 => $_POST["Harga"],
		":stok"		 => $_POST["Stok"],
		":satuan"	 => $_POST["Satuan"],
		":foto"      => $foto
	));
}else{
	$sql = "UPDATE tb_menu SET nama_menu=:nama_menu, kategori=:kategori, harga=:harga, stok=:stok, satuan=:satuan WHERE id_menu=:edit";
	$db->Execute($sql,array(
		":nama_menu" => $_POST["namaMenu"],
		":kategori"	 => $_POST["Kategori"],
		":harga"	 => $_POST["Harga"],
		":stok"		 => $_POST["Stok"],
		":satuan"	 => $_POST["Satuan"],
		":edit"		 => $_POST["edit"]
	));
	//jika ada perubahan foto saja
	if($foto != ""){
		$db->Execute("UPDATE tb_menu SET foto=:foto WHERE id_menu=:edit", array(":foto"=>$foto,":edit"=>$_POST["edit"]));
	}
}



/*
$namaMenu = $_POST["namaMenu"];
$kategori = $_POST["Kategori"];
$harga    = $_POST["Harga"];
$stok 	  = $_POST["Stok"];
$satuan   = $_POST["Satuan"];

$koneksi->query("INSERT INTO tb_menu (nama_menu,kategori,harga,stok,satuan) VALUES ('$namaMenu','$kategori',$harga,$stok,'$satuan')");
*/

//redirect to latihan2.php
header("location:latihan2.php");
