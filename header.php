<div class="my-header">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 my-logo">
				<!--ini bagian logo-->
				<img src="images/logo.png">
			</div>
			<div class="col-sm-6 my-alamat">
				<!--ini bagian alamat-->
				<h5>Telp : (0351) 483778</h5>
				<h6>Jl. Thamrin 35a - Madiun</h6>
				<img class="my-logo-sosmed" src="images/logo-fb.png">
				<img class="my-logo-sosmed" src="images/logo-twitter.jpg">
				<img class="my-logo-sosmed" src="images/logo-ig.jpg">
			</div>
		</div>
	</div>	
</div>

<div class="my-menu">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- ini tempat menu -->

				<ul id="navigation" class="nav">
				  <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
				  <li class="nav-item"><a class="nav-link" href="products.php">Products</a></li>
				  <li class="nav-item"><a class="nav-link" href="blog.php">Blogs</a></li>
				  <li class="nav-item"><a class="nav-link" href="about.php">About</a></li>
				</ul>

			</div>
		</div>
	</div>
</div>