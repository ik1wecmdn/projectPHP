<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Products - Wearnes Informatika 1</title>
    <!-- css bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- additional css lib -->
    <!--<link rel="stylesheet" href="css/slimmenu.min.css">-->
    <link rel="stylesheet" href="css/slicknav.min.css">
    
    <!-- tambah custom css di sini-->
    <link rel="stylesheet" href="css/my-style.css">
    
    <!-- tambahkan jquery -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <!-- tambahkan popper untuk dropdown -->
	<script src="js/popper.min.js"></script>
    <!-- tambahkan js bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- additional lib -->
    <!--<script src="js/jquery.slimmenu.min.js"></script>-->
    <script src="js/jquery.slicknav.min.js"></script>
    
  </head>
  <body>
  	
  	<?php include("header.php") ?>
  	
  	
  	
	<div class="my-product">
		<div class="container">
			<div class="row">
				<?php
				//ambil data dari mysql
				require("koneksi.php");
	  			$db = new MyDatabase();
	  			$dtMenu = $db->GetData("SELECT * FROM tb_menu");
				foreach($dtMenu as $menu){
				?>
				<div class="col-md-4">
					<!--product col 1-->
					<div class="my-kotak">
						<img src="latihan4getimagemenu.php?id=<?php echo $menu['id_menu']; ?>">
						<h4><?php echo $menu['nama_menu']; ?></h4>
						<p>Rp <?php echo number_format($menu['harga']); ?></p>
						<a class="btn btn-primary" href="products_detail.php?id=<?php echo $menu['id_menu']; ?>">Read More</a>
					</div>
				</div>
				
				<?php
				} //tutupe foreach
				?>
				
			</div>
		</div>
	</div>
  	
  	
  	
  	<div class="my-footer">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-12">
  					<!--footer-->
  					&copy; 2017 by informatika 1 wearnes education center madiun
  				</div>
  			</div>
  		</div>
  	</div>
  	
  	
  	
  </body>
</html>
