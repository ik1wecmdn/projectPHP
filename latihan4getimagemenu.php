<?php
	//id foto yang diambil
	$id = $_GET["id"];
	require("koneksi.php");
	//buat query ambil foto berdasarkan id_menu
	$db = new MyDatabase();
	$dtMenu = $db->GetData("SELECT foto FROM tb_menu WHERE id_menu=:id",array(":id" => $id));
	//ambil baris pertama 
	$menu = $dtMenu[0];
	
	//agar tampil dalam bentuk foto
	header("Content-Type: image/jpeg");
	if($menu["foto"] != ""){
		//jika foto di tabel mysql tidak kosong decode base64 menjadi gambar
		echo base64_decode($menu["foto"]) ;
	}else{
		//jika foto di tabel kosong maka ambil dari gambar no image yang ada di folder images
		echo file_get_contents("images/no_image_available.jpg") ;
	}
	
?>