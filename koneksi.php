<?php

class MyDatabase
{
	public $username = "ik1";
	public $password = "informatika";
	public $host 	 = "localhost";
	public $dbname   = "db_rminformatika";
	
	//funtion untuk ambil data - SELECT
	public function GetData($query, $myParams = array()){
		//open koneksi ke mysql
		$koneksi = new PDO("mysql:host=$this->host;dbname=$this->dbname",
						   $this->username, $this->password);
		//execute query
		$query = $koneksi->prepare($query);
		$query->execute($myParams);
		$result = $query->fetchAll();
		
		//tutup koneksi
		$koneksi = null;
		
		return $result;
	}
	
	
	//function untuk execute non query - INSERT UPDATE DELETE
	public function Execute($query, $myParams = array()){
		//open koneksi
		$koneksi = new PDO("mysql:host=$this->host;dbname=$this->dbname", 
						   $this->username, $this->password);
		//execute query
		$query = $koneksi->prepare($query);
		$query->execute($myParams);
		
		//tutup koneksi
		$koneksi = null;
	}
}

