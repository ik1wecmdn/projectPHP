<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Blog - Informatika 2017</title>
    <!-- css bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- additional css lib -->
    <!--<link rel="stylesheet" href="css/slimmenu.min.css">-->
    <link rel="stylesheet" href="css/slicknav.min.css">
    
    <!-- tambah custom css di sini-->
    <link rel="stylesheet" href="css/my-style.css">
    
    <style>
		.my-content{
			margin-top:40px;
		}
	</style>
    
    <!-- tambahkan jquery -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <!-- tambahkan popper untuk dropdown -->
	<script src="js/popper.min.js"></script>
    <!-- tambahkan js bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- additional lib -->
    <!--<script src="js/jquery.slimmenu.min.js"></script>-->
    <script src="js/jquery.slicknav.min.js"></script>
    
  </head>
  <body>
  	
  	<?php include("header.php") ?>
  	
  	
  	<div class="my-content">
  		<div class="container">
  			<div class="row">
  				
  				<div class="col-md-8">
  					<!-- content utama -->
  					
  					<div class="my-main-content">
  						<h1>Wellcome to My Company</h1>
  						<img src="images/logo-wearnes.jpg">
  						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
  					</div>
  					
  				</div>
  				<div class="col-md-4">
  					<!-- side menu -->
  					
  					<div class="my-side-menu">
  						<h2>LATEST NEWS</h2>
  						<a href="#">We will make best software</a>
  						<p>21 Maret 2005</p>
  						<img src="images/software.jpg">
  						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
  					</div>
  					
  					
  				</div>
  			</div>
  		</div>
  	</div>
  	
  	<div class="my-footer">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-12">
  					<!--footer-->
  					&copy; 2017 by informatika 1 wearnes education center madiun
  				</div>
  			</div>
  		</div>
  	</div>
  	
  	
  	
  </body>
</html>
